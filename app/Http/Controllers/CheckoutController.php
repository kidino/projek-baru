<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Plan;
use App\Models\User;
use App\Models\Payment;
use App\Models\Membership;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckoutController extends Controller
{

    /**
     * public function index() -- used in routing 
     * -- to redirect to payment gateway
     * 
     * public function verify() -- used in routing
     * -- returning from payment gateway 
     * 
     * protected function record_payment() 
     * -- to create a payment record before going to payment gateway 
     * 
     * protected function process_payment() 
     * -- to process payment & membership after returning from payment gateway
     * 
     * protected function {payment-gateway}_go() 
     * -- called by index() based on selected payment gateway
     * 
     * protected function {payment-gateway}_verify() 
     * -- called by verified based on selected payment gateway 
     */



    /**
     * 
     * /checkout/{plan}/{payment_gateway}
     */ 

    protected const PG_NAMESPACE = "\\App\\Libraries\\PaymentGateway\\";

    public function index(Request $request, Plan $plan, $payment_method) {

        // dd($plan, $payment_method);

        if(!in_array($payment_method, Payment::SUPPORTED_PAYMENTS)) {
            abort(405, 'Unknown payment method');
        }

        $payment = $this->record_payment($plan, $payment_method);

        $pg_classname = self::PG_NAMESPACE . ucfirst( $payment_method ); 

        $payment_gateway = new $pg_classname();
        $redirect_url = $payment_gateway->go($plan, $payment);

        return redirect( $redirect_url );

    }

    protected function record_payment(Plan $plan, $payment_method) {
        $user = Auth::user();

        $membership = $user->memberships()->first();

        if(!$membership) {
            $membership = new Membership();
            $membership->plan_id = $plan->id; 
            $membership->user_id = $user->id;
            $membership->status = 'inactive';
            $membership->expire_on = \Carbon\Carbon::today();
            $membership->save();
        }

        return Payment::create([
            'membership_id' => $membership->id,
            'plan_id' => $plan->id,
            'user_id' => $user->id,
            'amount' => $plan->price,
            'payment_method' => $payment_method,
            'payment_status' => 'pending',
            'payment_code' => Str::random(20),
            'remarks' =>  ucfirst($payment_method). ' payment for '. $plan->code
        ]);
    }


    /* /verify/{payment}/{payment_gateway}
    * 
    */

    public function verify(Request $request, Payment $payment, $payment_method) {

        if(!in_array($payment_method, Payment::SUPPORTED_PAYMENTS)) {
            abort(405, 'Unknown payment method');
        }

        $pg_classname = self::PG_NAMESPACE . ucfirst( $payment_method ); 
        $payment_gateway = new $pg_classname();

        $result = $payment_gateway->verify( $request, $payment );

        if($result['success'] && ($payment->payment_status == 'pending')) {
            $this->process_payment($payment);
        }

        return view('checkout.thankyou');
    }

    protected function process_payment(Payment $payment) {

        $user = User::find( $payment->user_id );
        $plan = Plan::find( $payment->plan_id );
        $membership = $user->memberships()->first();
        $membership->plan_id = $plan->id;

        if( Carbon::parse( $membership->expire_on )->isFuture() ) {
            $membership->expire_on = Carbon::parse( $membership->expire_on )->add( $plan->duration );
        } else {
            $membership->expire_on = Carbon::today()->add( $plan->duration );
        }

        $membership->status = 'active';
        $membership->save();

        $payment->payment_status = 'completed';
        $payment->save();

        return true;

    }

}
