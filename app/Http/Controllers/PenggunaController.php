<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use App\Models\Plan;
use App\Models\User;
use App\Models\Country;
use App\Models\Payment;
use App\Models\Membership;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class PenggunaController extends Controller
{
    /**
     * Display a listing of the resource.
     */

     public function index() {
        $users = User::paginate( 20 );
        return view('pengguna.index', compact('users') );
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('pengguna.create');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validation_rules = [
            'name' => 'required|string|min:5|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|min:8',
            'role' => 'required'
        ];

        $validated_data = $request->validate( $validation_rules );

        User::create( $validated_data );

        return redirect('/pengguna')->with('success', 'New user has been added');
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        echo "<h1>Maklumat Pengguna</h1>";
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {

        $user = User::findOrFail( $id );
        $role_options = User::USER_ROLES;
        $membership_status_options = Membership::MEMBERSHIP_STATUS;

        $countries = [ '' => 'Sila pilih negara' ] + Country::pluck('name', 'id')->toArray();

        $plan_options = [ '' => 'Sila pilih pelan' ] + Plan::getFormattedPlans();

        $membership_is_empty = $user->memberships->isEmpty();

        $payment_methods = [ '' => 'Pilih kaedah bayaran' ] + Payment::PAYMENT_METHODS;

        $js_plans = Plan::getIdPriceJSON();

        return view('pengguna.edit', compact('user', 'role_options', 'plan_options', 
        'membership_status_options', 'countries', 'membership_is_empty', 'payment_methods', 
        'js_plans'));
    }

    public function update_keahlian(Request $request, User $user) {
        $validated_data = $request->validate([
            'plan_id' => 'required',
            'expire_on' => 'required|date',
            'status' => 'required|string'
        ]);

        $membership = $user->memberships()->first();

        if(!$membership) {
            $membership = new Membership();
            $membership->user_id = $user->id;
        }

        $membership->plan_id = $request->input('plan_id');
        $membership->expire_on = Carbon::parse( $request->input('expire_on') );
        $membership->status = $request->input('status');

        $membership->save();

        return redirect()->back()->with('success', 'Membership details has been updated');
    }

    public function tambah_bayaran( Request $request, User $user) {
        $validated_data = $request->validate([
            'plan_id' => 'required',
            'payment_method' => 'required|string',
            'amount' => 'nullable|numeric',
            'new_expiry' => 'nullable|date',
            'remarks' => 'nullable|string'
        ]);

        $update_plan_id = 0;

        if(is_numeric($validated_data['plan_id'])) {
            $plan = Plan::findOrFail($validated_data['plan_id']);
            $update_plan_id = $plan->id;
        }

        $membership = $user->memberships()->first();

        if(!$membership) {
            $membership = new Membership();
            $membership->user_id = $user->id;
            $membership->status = 'active';
        }

        $membership->plan_id = $update_plan_id;

        if($membership->plan_id === 0) {
            $membership->expire_on = Carbon::parse( $validated_data['new_expiry'] );
        } else {
            if( Carbon::parse( $membership->expire_on )->isFuture() ) {
                $membership->expire_on = Carbon::parse($membership->expire_on)->add( $plan->duration );
            } else {
                $membership->expire_on = Carbon::today()->add( $plan->duration );
            }
        }

        $membership->save();

        $payment = new Payment();

        $payment->membership_id = $membership->id; 
        $payment->amount = ( $update_plan_id === 0 ) ? $validated_data['amount'] : $plan->price;
        $payment->payment_method = $validated_data['payment_method'];
        $payment->remarks = $validated_data['remarks'];
        $payment->payment_status = 'completed';
        $payment->payment_code = Str::random(20);

        $payment->save();

        return redirect()->back()->with('success', 'New payment has been recorded');

    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        $user = User::findOrFail( $id );

        $validation_rules = [
            'name' => 'required|string|min:5|max:255',
            'email' => 'required|email|max:255|unique:users,email,'.$user->id,
            'role' => 'required',
            'address_1' => 'required|string|max:255',
            'address_2' => 'nullable|string|max:255',
            'city' => 'nullable|string|max:255',
            'state' => 'nullable|string|max:255',
            'postcode' => 'nullable|string|max:10',
            'country_id' => 'required|integer'
        ];

        if( strlen( $request->input('password')) > 0 ) {
            $validation_rules['password'] = 'min:8';
        }

        // dd($validation_rules);

        $validated_data = $request->validate( $validation_rules );

        $user->fill( $validated_data );

        if(strlen( $request->input('password')) > 0) {
            $user->password = Hash::make( $request->input('password') );
        }

        $user->save();

        return redirect('/pengguna/' . $user->id .'/edit')->with('success', 'User has been updated');
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
