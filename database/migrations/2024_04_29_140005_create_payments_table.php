<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('payments', function (Blueprint $table) {
            $table->id();

            $table->integer('membership_id');
            $table->integer('amount');
            $table->string('payment_method', 20);
            $table->string('payment_status', 20);
            $table->string('payment_code', 20);
            $table->text('remarks');

            $table->timestamps();

            $table->unique('payment_code');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('payments');
    }
};
