<div class="mb-3">
    <label for="{{ $id }}" class="form-label">{{ $label }}</label>

    <select name="{{$name}}" id="{{$id}}" 
        class="form-select @if(!empty($error)) is-invalid @endif ">

        @foreach( $options as $key => $option ) 
            <option value="{{$key}}"
            @if($value == $key) selected @endif
            >{{$option}}</option>
        @endforeach

    </select>

    @if(!empty($error)) 
        <div class="invalid-feedback">{{ $error }}</div>
    @endif
</div>