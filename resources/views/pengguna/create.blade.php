@extends('layouts.dashboard')


@section('page-pretitle', 'Admin')
@section('page-title', 'Pengguna Baru')
@section('page-title-actions')
    <!-- Page title actions -->
    <div class="col-auto ms-auto d-print-none">
        <div class="btn-list">

        </div>
    </div>
@endsection

@section('content')


<div class="container-xl">
        <div class="row justify-content-center">
            <div class="col">



    @if(session('success'))  
        <div class="alert alert-success mt-3">{{ session('success') }}</div>
    @endif 


    <div class="card">
        <div class="card-body">

        <form action="/pengguna" method="post">
@csrf


    <div class="mb-3">
        <label for="" class="form-label">Nama</label>
        <input type="text" 
        class="form-control @error('name')) is-invalid @enderror" 
        name="name"
        value="{{ old('name') }}">
        @error('name')
        <div class="invalid-feedback">{{ $message }}</div>
        @enderror 
    </div>

    <div class="mb-3">
        <label for="" class="form-label">Email</label>
        <input type="email" 
        class="form-control @error('email')) is-invalid @enderror" 
        name="email"
        value="{{ old('email') }}" >
        @error('email')
        <div class="invalid-feedback">{{ $message }}</div>
        @enderror         
    </div>

    <div class="mb-3">
        <label for="" class="form-label">Password</label>
        <input type="password" 
        class="form-control @error('password')) is-invalid @enderror" 
        name="password"
        value="{{ old('password') }}">
        @error('password')
        <div class="invalid-feedback">{{ $message }}</div>
        @enderror   
    </div>


    <div class="mb-3 @error('role') is-invalid @enderror">
                <label for="" class="form-label">Role</label>

                <div class="form-check">
                    <input class="form-check-input" type="radio" 
                    id="role-member" name="role" value="member"
                    @if(old('role') == 'member')
                        checked
                    @endif 
                    />
                    <label class="form-check-label" for="role-member">
                        Member
                    </label>
                </div>

                <div class="form-check">
                    <input class="form-check-input" type="radio" 
                    id="role-admin" name="role" value="admin"      
                    
                    @if(old('admin') == 'member')
                        checked
                    @endif                     
                    />
                    <label class="form-check-label" for="role-admin">
                        Admin
                    </label>
                </div>                
            </div>
            @error('role')
                <div class="invalid-feedback">{{ $message }}</div>
            @enderror    

    <button class="btn btn-primary">Simpan</button>

</form>


        </div>

    </div><!-- /.card -->

    </div> <!-- /.col -->
    </div> <!-- /.row -->
    </div> <!-- /.container -->

@endsection




